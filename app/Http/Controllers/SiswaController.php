<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SiswaController extends Controller
{
    public function get_siswa(){
        $get_siswa = DB::table('siswa')->get();
        return response()->json($get_siswa);
    }
}

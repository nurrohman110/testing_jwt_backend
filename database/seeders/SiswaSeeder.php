<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Str;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswa')->truncate();
        for($i = 0; $i < 1000; $i++){
            DB::table('siswa')->insert([
                'nama' => Str::random(10),
                'alamat' => Str::random(100),
            ]);
        }
    }
}
